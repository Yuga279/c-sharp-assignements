﻿using System;
namespace Csharp;
class program
{
public static void Main(String[] args)
        {
// Write a program and ask the user to continuously enter a number or type "Quit" to exit. 
//The list of numbers may include duplicates. 
// Display the unique numbers that the user has entered.

    /*var userInput = new List<int>();
            while (userInput.Count < 5)
            {
                Console.Write("Enter a number: ");
                var rNum = Convert.ToInt32(Console.ReadLine());
                if (userInput.Contains(rNum))
                {
                    Console.WriteLine("Repeated Number" + rNum);
                    continue;
                }
                userInput.Add(rNum);
            }
            userInput.Sort();
            foreach (var rNum in userInput)
                Console.WriteLine(rNum);*/

// Write a program and ask the user to enter 5 numbers. If a number has been previously entered, 
// display an error message and ask the user to re-try. 
// Once the user successfully enters 5 unique numbers, sort them and display the result on the console.

                /*var num = new List<int>();
            
            while (true)
            {
                Console.Write("Enter a number: or Exit to close ");
                var userInput = Console.ReadLine();

                if (userInput.ToLower() == "Exit")
                    break;

                num.Add(Convert.ToInt32(userInput));
            }

            var uniques = new List<int>();
            foreach (var number in num)
            {
                if (uniques.Contains(number))
                    uniques.Add(number);
            }

            Console.WriteLine("Unique numbers:");
            foreach (var number in uniques)
                Console.WriteLine(number);*/

// Write a program and ask the user to supply a list of comma separated numbers (e.g 5, 1, 9, 2, 10). 
// If the list is empty or includes less than 5 numbers, 
// display "Invalid List" and ask the user to re-try; otherwise, display the 3 smallest numbers in the list.

            /*string[] elements;
            while (true)
            {
                Console.Write("Enter the CS numbers:");
                var input = Console.ReadLine();

                if (!String.IsNullOrWhiteSpace(input))
                {
                    elements = input.Split(',');
                    if (elements.Length >= 5)
                        break;
                }

                Console.WriteLine("Invalid");
            }

            var numbers = new List<int>();
            foreach (var number in elements)
                numbers.Add(Convert.ToInt32(number));

            var smallests = new List<int>();
            while (smallests.Count < 3)
            {
                // Assume the first number is the smallest
                var min = numbers[0];
              
                foreach (var number in numbers)
                {
                    if (number < min)
                        min = number;
                }
                smallests.Add(min);
                numbers.Remove(min);
            }

            Console.WriteLine("smallest numbers are: ");
            foreach (var number in smallests)
                Console.WriteLine(number);*/

// Write a program and ask the user to enter their name. Use an array to reverse the name and then store the result in a new string. 
// Display the reversed name on the console.

            /*Console.Write("Name");
            var name = Console.ReadLine();

            var input = new char[name.Length];
            for (var i = name.Length; i > 0; i--)
                input[name.Length - i] = name[i - 1];

            var reversed = new string(input);
            Console.WriteLine("Reversed name: " + reversed);*/

// When you post a message on Facebook, depending on the number of people who like your post,
// Facebook displays different information.
// var names = new List<string>();
//             while (true)
//             {
//                 Console.Write("Type a name: ");
//                 var input = Console.ReadLine();
//                 if (String.IsNullOrWhiteSpace(input))
//                     break;
//                 names.Add(input);
//             }
//             if (names.Count > 2)
//                 Console.WriteLine("{0}, {1} and {2} others like your post", names[0], names[1], names.Count - 2);
//             else if (names.Count == 2)
//                 Console.WriteLine("{0} and {1} like your post", names[0], names[1]);
//             else if (names.Count == 1)
//                 Console.WriteLine("{0} likes your post.", names[0]);
//             else
//                 Console.WriteLine();
//             Console.Write("Enter a String : ");
//             string name = Console.ReadLine();
//             string reverse = string.Empty;
//             foreach (char c in name)
//             {
//                 reverse = c + reverse;
//             }
//             Console.WriteLine($"The Reverse string is : {reverse}");
//             Console.ReadKey();
     }
}
    
